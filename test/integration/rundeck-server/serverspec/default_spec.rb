#
# Copyright (c) 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'

describe file('/etc/rundeck/realm.properties') do
  it { should contain('admin=password,admin,user') }
  it { should contain('user1=password1,group1,user') }
  it { should_not contain('user2=password2,group2,user') }
  it { should_not contain('user3=,group1,user') }
end

project_config = <<-PROJECT_CONFIG.gsub(/^ */, '')
  project.name=test
  project.ssh-authentication=privateKey
  project.ssh-keypath=/var/lib/rundeck/.ssh/id_rsa
  resources.source.1.config.cache=true
  resources.source.1.config.file=/var/rundeck/projects/test/etc/resources.xml
  resources.source.1.config.timeout=30
  resources.source.1.type=file
  service.FileCopier.default.provider=jsch-scp
  service.NodeExecutor.default.provider=jsch-ssh
PROJECT_CONFIG

describe file('/var/rundeck/projects/test/etc/project.properties') do
  it { should contain(project_config) }
end

describe file('/var/rundeck/projects/test/etc/resources.xml') do
  [
    'rundeck-server-centos-7',
    'test1.example.com',
    'test2.example.com'
  ].each do |host|
    host_line =
      ["name=\"#{host}\"", "hostname=\"#{host}\"", 'username="rundeck"']
    host_line.each { |attr| it { should contain(attr) } }
  end
end

describe file('/etc/rundeck/tokens.properties') do
  it { should contain('admin: hxCWVzuqk2N1bVJ1Gj57EtC8f7C12By5,admin,user') }
end

job_config = {
  'sequence' => {
    'keepgoing' => false,
    'stategy' => 'node-first',
    'commands' => [{ 'exec' => 'uname -a' }]
  },
  'nodefilters' => { 'dispatch' => { 'threadcount' => 10 } }
}

describe file('/var/rundeck/projects/test/etc/uname.yml') do
  it { should contain(job_config.to_yaml) }
end
