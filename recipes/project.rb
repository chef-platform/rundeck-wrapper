#
# Copyright (c) 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

::Chef::Recipe.send(:include, ClusterSearch)

# Deploy rundeck projects configuration
node[cookbook_name]['projects'].each do |project, project_config|
  nodes = project_config['nodes'].map do |_, config|
    cs_config = config.is_a?(Hash) ? config : { 'role' => config }
    cluster = cluster_search(cs_config)
    next if cluster.nil? || cluster['hosts'].empty?

    cluster['hosts'].map do |fqdn|
      { 'name' => fqdn, 'hostname' => fqdn, 'username' => config['user'] }
    end
  end.flatten.compact

  rundeck_server_project project do
    executor project_config['executor'].to_sym
    sources project_config['sources']
    nodes nodes
    cookbook cookbook_name
  end
end
