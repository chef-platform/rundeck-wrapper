#
# Copyright (c) 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Retrieve realm users credentials and optional token from data bag
data_bag_name = node[cookbook_name]['data_bag']['realm_users']
realm_properties = {}
token_properties = {}

data_bag(data_bag_name).each do |item|
  user = data_bag_item(data_bag_name, item)
  next if (node[cookbook_name]['whitelist'] & user['groups']).empty?\
          || user['rundeck_password'].nil?

  groups = user['groups'].join(',') << ",#{node['rundeck_server']['rolename']}"
  realm_properties[item] = "#{user['rundeck_password']},#{groups}"
  token = user['rundeck_token']
  token_properties[item] = "#{token},#{groups}" unless token.nil?
end

resource = resources(template: ['realm.properties'])
resource.variables(properties: realm_properties)

# Deploy Rundeck tokens file
header = "# Produced by Chef -- changes will be overwritten\n"
content = token_properties.map { |h, v| "#{h}: #{v}" }
file_path =
  node['rundeck_server']['rundeck-config.framework']['rundeck.tokens.file']

file 'rundeck_tokens_properties' do
  path file_path
  content "#{header}\n#{content.join("\n")}\n"
  mode '0644'
  owner 'rundeck'
  group 'rundeck'
  not_if { file_path.nil? }
end
