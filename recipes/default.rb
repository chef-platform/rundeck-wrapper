#
# Copyright (c) 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

include_recipe "#{cookbook_name}::java"
include_recipe 'rundeck-server::default'
include_recipe "#{cookbook_name}::user"
include_recipe "#{cookbook_name}::project"
include_recipe "#{cookbook_name}::job"

# Disable API check
resource = resources(execute: 'ensure api is up')
resource.action(:nothing)

# Disable resources to supports Rundeck 3.x
resource = resources(ruby_block: 'web-xml-update')
resource.action(:nothing)

resource = resources(file: 'rundeck-quartz-properties')
resource.action(:nothing)

# Update profile template to supports Rundeck 3.x
resource = resources(template: ['rundeck-profile'])
resource.cookbook = cookbook_name
resource.variables(basedir: node['rundeck_server']['basedir'],
                   confdir: node['rundeck_server']['confdir'],
                   jvm: node['rundeck_server']['jvm'])
