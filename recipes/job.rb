#
# Copyright (c) 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'net/http'
require 'uri'

endpoint =
  node['rundeck_server']['rundeck-config.framework']['framework.server.url']
project_dir = node['rundeck_server']['jvm']['Drdeck.projects']

user = data_bag_item(
  node[cookbook_name]['data_bag']['realm_users'],
  node[cookbook_name]['api_user']
)

# Deploy rundeck jobs configuration

# rubocop:disable Metrics/BlockLength
node[cookbook_name]['projects'].each do |project, config|
  next unless config.key?('jobs')

  config['jobs'].each do |job, job_config|
    file_flag = "#{project_dir}/#{project}/etc/#{job}.api-success"

    file "#{project_dir}/#{project}/etc/#{job}.yml" do
      content job_config.to_h.to_yaml
      owner 'rundeck'
      group 'rundeck'
      notifies :delete, "file[#{file_flag}]", :immediately
      notifies :create, "rundeck_server_job[#{job}]", :immediately
    end

    rundeck_server_job job do
      project project
      config job_config
      endpoint endpoint
      api_token user['rundeck_token']
      action :nothing
      notifies :create, "file[#{file_flag}]", :immediately
      retries 5
      retry_delay 20
    end

    file file_flag do
      content ''
      owner 'rundeck'
      group 'rundeck'
      action :nothing
    end

    log "Retry rundeck_server_job[#{job}]" do
      notifies :create, "rundeck_server_job[#{job}]", :immediately
      not_if { ::File.exist?(file_flag) }
    end
  end
end
# rubocop:enable Metrics/BlockLength
