Changelog
=========

2.0.0
-----

Breaking changes:
- This cookbook no longer supports old rundeck version (< 3.0.0)

Main:

- feat: supports rundeck 3.x
  + update version 2.10.8-1.50.GA to 3.0.7.20181008-1.201810082316
  + disable web-xml-update ruby block resource
  + disable rundeck-quartz-properties file resource
  + update rundeck-server template profile file
  + configure thread pool
  + fix log4j path

1.2.0
-----

Breaking changes:

- feat: rename secret recipe to user

Main:

- feat: add recipe to handle jobs creation
- feat: add recipe to handle projects creation
- feat: generate token from realm users data bag

Test:

- test: include .gitlab-ci.yml from test-cookbook
- test: replace deprecated require_chef_omnibus

Misc:

- doc: use doc in git message instead of docs
- doc: update data bag def and wrapper desc
- doc: add rundeck_password to data bag definition
- chore: add supermarket category in .category
- chore: set generic maintainer & helpdesk email
- chore: add 2018 to copyright notice
- style(rubocop): fix empty line after guard clause
- style: wrap resources template into 80 columns

1.1.0
-----

Main:

- disable useless "api check" that execute at each run
- do not create user without rundeck password

1.0.0
-----

- Initial version with centos support
