#
# Copyright (c) 2017-2018 Make.org
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

cookbook_name = 'rundeck-wrapper'

# Version of Rundeck packages
default['rundeck_server']['packages'] = {
  'rundeck' => '3.0.7.20181008-1.201810082316',
  'rundeck-config' => '3.0.7.20181008-1.201810082316'
}

# Encrypted data bag to manage realm users credentials
default[cookbook_name]['data_bag']['realm_users'] = 'rundeck_users'

# Only users belonging to theses groups will be created
default[cookbook_name]['whitelist'] = []

# Which Rundeck user should interact with API to handle job creation
default[cookbook_name]['api_user'] = nil

# Project configuration
#
# Each hash key represents a project configuration
# Top hash keys are project names
#
# Each project configuration should be an hash with following keys :
# - executor # Executor name
# - sources # List of nodes sources
# - jobs # List of projects configuration
#
# 'executor' and 'sources' keys will be directly mapped to a
# rundeck-server_project resource.
#
# See http://github.com/criteo-cookbooks/rundeck-server#rundeck-server_project
# for more information on this resource.
#
# If source type is file, you should add a 'nodes' keys in which each item
# must contains a ClusterSearch hash configuration and the user associated
# to reach nodes resulting from search.
#
# See https://gitlab.com/chef-platform/cluster-search for more information
# about configure ClusterSearch.
#
# 'jobs' should be an hash with jobs name as top keys and hash version of yaml
# rundeck project configuration format as value.
#
# For example:
#
#  'projects' => {
#    'project_name' => {
#      'executor' => 'ssh',
#      'sources' => [{
#        'type' => 'file',
#        'config.timeout' => 0,
#        'config.cache' => true
#      }],
#      'nodes' => {
#        'group_name_key' => {
#          'role' => 'role_of_nodes',
#          'user' => 'user_with_access_to_nodes'
#        }
#      },
#      'jobs' => {
#        'job_name' => {
#          'sequence' => {
#            'commands' => [{
#              'exec' => 'cmd_to_exec'
#            }],
#            'keepgoing' => false,
#            'strategy' => 'node-first'
#          },
#          'nodefilters' => {
#            'dispatch' => {
#              'threadcount' => 10
#            }
#          },
#          'filter' => '.*'
#        }
#      }
#    }
#  }
#
# See also .kitchen.yml for another example
default[cookbook_name]['projects'] = {}

# Do no use java cookbook, use package directly
default['rundeck_server']['install_java'] = false
default[cookbook_name]['java'] = {
  'centos' => 'java-1.8.0-openjdk-headless'
}

# XX:MaxPermSize is now named XX:MaxMetaspaceSize
default['rundeck_server']['jvm']['XX:MaxMetaspaceSize'] = '256m'

# Configure thread pool and fix log4j path
properties = 'rundeck-config.properties'
default['rundeck_server'][properties]['quartz.props.threadPool.threadCount'] =
  10

# Fix log4j path
default['rundeck_server'][properties]['rundeck.log4j.config.file'] =
  '/etc/rundeck/log4j.properties'

# Configure retries for the package resources, default = global default (0)
# (mostly used for test purpose)
default[cookbook_name]['package_retries'] = nil
