{
  'id' => 'admin',
  'rundeck_password' => {
    'encrypted_data' => '5FhGiG+tVBpdExwKepwXwZ6dxTXvy1lQTklX\n',
    'iv' => 'UdyIWTr3+HFsbBwD\n',
    'auth_tag' => 'VXezpvJQiI/AocP0r+nQZg==\n',
    'version' => 3,
    'cipher' => 'aes-256-gcm'
  },
  'groups' => {
    'encrypted_data' => 'epcD6ViEMcoAmIB0LwYdkD1xC+oK0Z8Flj0=\n',
    'iv' => 'rOsyUO1Tdsdtj5RW\n',
    'auth_tag' => 'E8L4bJbH/LOglJbrB5zvkg==\n',
    'version' => 3,
    'cipher' => 'aes-256-gcm'
  },
  'rundeck_token' => {
    'encrypted_data' =>
      'ZGgZnZHe6dsDvYFyvNk+X2Z+U98gJ7cWr6YWfrVT0dtSNAk4D3bNNHvehHoE3LgOnbnC\n',
    'iv' => 'tEJL4JN+UBy4gav1\n',
    'auth_tag' => 'i9bSGBPf4muqADjAre9SKw==\n',
    'version' => 3,
    'cipher' => 'aes-256-gcm'
  }
}
